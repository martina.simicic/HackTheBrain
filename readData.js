var PORT = 5000;
var HOST = '127.0.0.1';

var dgram = require('dgram');
var message = new Buffer('/muse/version');

var client = dgram.createSocket('udp4');

client.on('message', function(msg, rinfo){
  if (msg.toString('utf-8', 0, 29) == '/muse/elements/alpha_relative') {
    console.log(msg.toString('utf-8', 33, 150));
  }
});

client.bind(PORT);
