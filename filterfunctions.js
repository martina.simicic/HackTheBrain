module.exports = {
  linkFilters: function (filterA, filtersB, echoToConsole) {
    filterA.on("readable", function(){
      var obj;
      while (null !== (obj = this.read())) {
        for (var i = 0; i < filtersB.length; i++) {
          filtersB[i].write(obj);
        };
        if (echoToConsole) {
          console.log(obj);
        }
      }
    });
  },

  linkMessageSourceToFilter: function (messageSource, filter, echoToConsole) {
    messageSource.on("message", function (message) {
      filter.write(message);
      if (echoToConsole) {
        console.log(message);
      }
    });
  },

  linkFilterToConsole: function (filter) {
    filter.on('readable', function(){
      var obj;
      while (null !== (obj = this.read())) {
        console.log(obj);
      }
    });
  },

  linkFilterToSerial: function (filter, serialPort, echoToConsole) {
    filter.on('readable', function(){
      var obj;
      while (null !== (obj = this.read())) {
        serialPort.write(obj);
        if (echoToConsole) {
          console.log(obj);
        }
      }
    });
  },
}