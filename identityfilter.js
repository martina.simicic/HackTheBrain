var stream = require('stream');
var util = require('util');

var Transform = stream.Transform ||
  require('readable-stream').Transform;

function IdentityFilter (options) {
  // allow use without new
  if (!(this instanceof IdentityFilter)) {
    return new IdentityFilter(options);
  }

  // init Transform
  if (!options) options = {}; // ensure object
  options.objectMode = true; // forcing object mode
  Transform.call(this, options);
}

util.inherits(IdentityFilter, Transform);

IdentityFilter.prototype._transform = function (obj, enc, cb) {
  this.push(obj);
  cb();
}

module.exports = IdentityFilter;
