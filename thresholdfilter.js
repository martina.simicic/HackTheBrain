var stream = require('stream');
var util = require('util');

var Transform = stream.Transform ||
  require('readable-stream').Transform;

function ThresholdFilter (thresholdValue, options) {
  // allow use without new
  if (!(this instanceof ThresholdFilter)) {
    return new ThresholdFilter(thresholdValue, options);
  }

  // init Transform
  if (!options) options = {}; // ensure object
  options.objectMode = true; // forcing object mode
  Transform.call(this, options);
  this.thresholdValue = thresholdValue;
}

util.inherits(ThresholdFilter, Transform);

ThresholdFilter.prototype._transform = function (obj, enc, cb) {
  this.push(obj.args[0] >= this.thresholdValue);
  cb();
}

module.exports = ThresholdFilter;
