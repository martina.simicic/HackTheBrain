int redLedPin = 7; // Am concentrated, do not disturb
int greenLedPin = 12; // Ok to disturb

int initialized = 0;
int concentrated = 0;

void setup () {
  Serial.begin(9600);
  
  pinMode(redLedPin, OUTPUT);
  pinMode(greenLedPin, OUTPUT);
}

void loop () {
  if (Serial.available() > 0) {
    //Serial.println("Serial.available() was true");
    char value = Serial.read();
    if (value == '1') {
      concentrated = 1;
    } else if (value == '0') {
      concentrated = 0;
    }
    initialized = 1;
  }
  
  if (initialized == 0) {
    // No information to show yet.
    digitalWrite(redLedPin, LOW);
    digitalWrite(greenLedPin, LOW);
  } else if (concentrated) {
    digitalWrite(redLedPin, HIGH);
    digitalWrite(greenLedPin, LOW);
  } else {
    digitalWrite(redLedPin, LOW);
    digitalWrite(greenLedPin, HIGH);
  }
}

