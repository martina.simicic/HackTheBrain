var osc = require("osc");
var SerialPort = require("serialport").SerialPort
var fs = require("fs");
var file = "development.db";
var exists = fs.existsSync(file);

if(!exists) {
  console.log("Creating DB file.");
  fs.openSync(file, "w");
}

var sqlite3 = require("sqlite3").verbose();
var db = new sqlite3.Database(file);

var filterFunctions = require("./filterfunctions");
var dateTime = require("./dateTime").getDateTime;

var AddressFilter = require("./addressfilter");
var ThresholdFilter = require("./thresholdfilter");
var AverageCollectFilter = require("./averagecollectfilter");
var IntervalFilter = require("./intervalfilter");
var IntervalThresholdFilter = require("./intervalthresholdfilter");
var IntegerFilter = require("./integerfilter");
var IdentityFilter = require("./identityfilter");

var brainPort = new osc.UDPPort({
  localAddress: "0.0.0.0",
  localPort: 5000
});

var WebSocketServer = require('ws').Server
  , wss = new WebSocketServer({ port: 8080 });

var serialPort = new SerialPort("/dev/ttyACM0", {
  baudrate: 9600
});

var rawValues = new AddressFilter('/muse/elements/experimental/concentration'); // Osc -> Osc
var averageValues = new AverageCollectFilter(10); // Osc -> Osc
var concentratedBoolValues = new ThresholdFilter(0.5); // Osc -> bool
var intervals = new IntervalFilter(); // bool -> Interval
var intervalsForDB = new IdentityFilter(); // Interval -> Interval (any type to itself)
var intervalsForBroadcasting = new IdentityFilter(); // Interval -> Interval (any type to itself)
var intervalsForArduino = new IdentityFilter(); // Interval -> Interval (any type to itself)
var filteredIntervals = new IntervalThresholdFilter(10); // Interval -> Interval
var intervalsForArduinoInt = new IntegerFilter(); // Interval -> String ('0' or '1')

filterFunctions.linkMessageSourceToFilter(brainPort, rawValues);
filterFunctions.linkFilters(rawValues, [averageValues]);
filterFunctions.linkFilters(averageValues, [concentratedBoolValues]);
filterFunctions.linkFilters(concentratedBoolValues, [intervals]);
filterFunctions.linkFilters(intervals, [filteredIntervals, intervalsForDB]);
filterFunctions.linkFilters(filteredIntervals, [intervalsForBroadcasting, intervalsForArduino]);
filterFunctions.linkFilters(intervalsForArduino, [intervalsForArduinoInt]);
filterFunctions.linkFilterToSerial(intervalsForArduinoInt, serialPort, true);

intervalsForDB.on("readable", function () {
  var interval;
  while (null !== (interval = this.read())) {
    var formattedTime = dateTime(interval.time);
    var formattedPreviousTime = dateTime(interval.previousTime);
    var state = interval.state;
    var previousState = interval.previousState;
    var previousDuration = interval.previousDuration / 1000; // milliseconds to seconds

    console.log("logging      [" + formattedTime + "] " + previousState + " (" + previousDuration + "s) -> " + state);

    writeToLogs(formattedPreviousTime, previousState, previousDuration);
  }
});

intervalsForBroadcasting.on("readable", function () {
  var interval;
  while (null !== (interval = this.read())) {
    var formattedTime = dateTime(interval.time);
    var formattedPreviousTime = dateTime(interval.previousTime);
    var state = interval.state;
    var previousState = interval.previousState;
    var previousDuration = interval.previousDuration / 1000; // milliseconds to seconds

    console.log("broadcasting [" + formattedTime + "] " + previousState + " (" + previousDuration + "s) -> " + state);

    broadcastInformation(state);
  }
});

function createConcentrationRecordsTable() {
  db.run("CREATE TABLE IF NOT EXISTS ConcentrationRecords (id INTEGER PRIMARY KEY, startAt TEXT, state TEXT, duration INTEGER)");
}

function writeToLogs(time, state, duration) {
  db.serialize(function() {
    createConcentrationRecordsTable();
    db.run("INSERT INTO ConcentrationRecords(startAt, state, duration) VALUES ('"+time+"', '"+state+"', "+duration+")");
  });
}

function broadcastInformation(state){
  wss.clients.forEach(function each(client) {
    client.send(state ? "true" : "false");
  });
}

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
  });

  ws.send('something');
});

brainPort.open();
