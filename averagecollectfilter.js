var stream = require('stream');
var util = require('util');

var Transform = stream.Transform ||
  require('readable-stream').Transform;

function AverageCollectFilter (nSamples, options) {
  // allow use without new
  if (!(this instanceof AverageCollectFilter)) {
    return new AverageCollectFilter(nSamples, options);
  }

  // init Transform
  if (!options) options = {}; // ensure object
  options.objectMode = true; // forcing object mode
  Transform.call(this, options);
  this.nSamples = nSamples; // Limit

  this.nSamplesCollected = 0; // Init private bookkeeping
  this.sum = 0.0; // Init private bookkeeping
}

util.inherits(AverageCollectFilter, Transform);

AverageCollectFilter.prototype._transform = function (obj, enc, cb) {
  this.sum += obj.args[0];
  this.nSamplesCollected++;
  if (this.nSamplesCollected >= this.nSamples) {
    // Push an aggregated object into the stream, re-use the current object

    // Calculate average
    obj.args[0] = this.sum / this.nSamplesCollected;
    this.sum = 0.0;
    this.nSamplesCollected = 0;

    this.push(obj);
  }
  cb();
}

module.exports = AverageCollectFilter;
