var stream = require('stream');
var util = require('util');

var Transform = stream.Transform ||
  require('readable-stream').Transform;

function IntervalThresholdFilter (durationThreshold, options) {
  // allow use without new
  if (!(this instanceof IntervalThresholdFilter)) {
    return new IntervalThresholdFilter(durationThreshold, options);
  }

  // init Transform
  if (!options) options = {}; // ensure object
  options.objectMode = true; // forcing object mode
  Transform.call(this, options);
  this.durationThreshold = durationThreshold  * 1000; // milliseconds internally

  // Initialize internal bookkeeping:
  this.currentInterval = null;
  this.currentTimeout = null;
}

util.inherits(IntervalThresholdFilter, Transform);

IntervalThresholdFilter.prototype._transform = function (interval, enc, cb) {
  if (this.currentInterval == null) {
    this.push(interval);
    this.currentInterval = interval;
  } else if (interval.state != this.currentInterval.state) {
    // We receive a new state but are not sure yet if it's worth keeping.
    // The duration of previous segment was too short but the new (future)
    // duration might be worth something.
      this.currentTimeout = setTimeout(function (thisFilter, thisInterval) {
        var sendInterval = thisInterval;
        sendInterval.previousDuration += thisFilter.currentInterval.previousDuration;
        thisFilter.push(sendInterval);
        thisFilter.currentInterval = thisInterval;
      }, this.durationThreshold, this, interval);
  } else {
    // Same interval received as what we already broadcasted
    // Not worth outputting
    this.currentInterval.previousDuration += interval.previousDuration;
    // Signal we received last is apparently bogus:
    clearTimeout(this.currentTimeout);
  }
  cb();
};

module.exports = IntervalThresholdFilter;
