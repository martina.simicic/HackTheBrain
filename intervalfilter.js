var stream = require('stream');
var util = require('util');

var Transform = stream.Transform ||
  require('readable-stream').Transform;

function IntervalFilter (options) {
  // allow use without new
  if (!(this instanceof IntervalFilter)) {
    return new IntervalFilter(options);
  }

  // init Transform
  if (!options) options = {}; // ensure object
  options.objectMode = true; // forcing object mode
  Transform.call(this, options);

  // Initialize internal bookkeeping:
  currentState = null;
  startDate = new Date();
}

util.inherits(IntervalFilter, Transform);

IntervalFilter.prototype._transform = function (booleanInput, enc, cb) {
  if (this.currentState == null) {
    this.currentState = booleanInput;
    this.startDate = new Date();
  } else if (this.currentState != booleanInput) {
    var now = new Date();
    var duration = now - this.startDate;
    var interval = {
      time: now,
      state: booleanInput,
      previousTime: this.startDate,
      previousState: this.currentState,
      previousDuration: duration,
    };
    this.push(interval);
    this.currentState = booleanInput;
    this.startDate = now;
  }
  cb();
}

module.exports = IntervalFilter;
