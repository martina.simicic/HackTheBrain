var stream = require('stream');
var util = require('util');

var Transform = stream.Transform ||
  require('readable-stream').Transform;

function AddressFilter (address, options) {
  // allow use without new
  if (!(this instanceof AddressFilter)) {
    return new AddressFilter(address, options);
  }

  // init Transform
  if (!options) options = {}; // ensure object
  options.objectMode = true; // forcing object mode
  Transform.call(this, options);
  this.address = address;

}

util.inherits(AddressFilter, Transform);

AddressFilter.prototype._transform = function (obj, enc, cb) {
  if (obj.address == this.address) {
    this.push(obj);
  }
  cb();
}

module.exports = AddressFilter;
