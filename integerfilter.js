var stream = require('stream');
var util = require('util');

var Transform = stream.Transform ||
  require('readable-stream').Transform;

function IntegerFilter (options) {
  // allow use without new
  if (!(this instanceof IntegerFilter)) {
    return new IntegerFilter(options);
  }

  // init Transform
  if (!options) options = {}; // ensure object
  options.objectMode = true; // forcing object mode
  Transform.call(this, options);
}

util.inherits(IntegerFilter, Transform);

IntegerFilter.prototype._transform = function (interval, enc, cb) {
  if (interval.state) {
    // Concentrated
    this.push('1');
  } else {
    // Not concentrated
    this.push('0');
  }
  cb();
}

module.exports = IntegerFilter;
